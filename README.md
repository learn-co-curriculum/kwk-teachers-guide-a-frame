## Objectives

1. Students will be introduced to A-Frame and threeJS
2. With guidance, students should be able to create interactive 3D environments in JavaScript

## Resources

* [Intro to A-Fram](https://aframe.io/docs/0.8.0/introduction/)

<p class='util--hide'>View <a href='https://learn.co/lessons/kwk-teachers-guide-a-frame'>KWK Teachers Guide A-Frame</a> on Learn.co and start learning to code for free.</p>
